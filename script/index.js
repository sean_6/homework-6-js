
"use strict";

const createNewUser = () => {
    
    let firstName = prompt("What is your name?")
    let lastName = prompt("what is your last name?")
    let birthday = prompt(`"What is your birthday? dd.mm.yyyy`)
    
    const newUser = {
        firstName,
        lastName,
        
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        birthday,
        
        getAge() {
            const year_in_ms = 1000 * 60 * 60 * 24 * 365; // ms * sec * min * hours * days
            return Math.floor((new Date() - new Date(birthday)) / year_in_ms)
        },
        
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6)
        }
    }
    return newUser
}

const user = createNewUser()

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
